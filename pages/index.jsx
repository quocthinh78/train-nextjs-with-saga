import Head from 'next/head'
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Table from '../components/Table';
import { getPosts,fetchPosts } from '../store/actions';
import {wrapper} from "./../store/store"
import {END} from "redux-saga"
 function Home() {
  const dispatch = useDispatch();
  const posts = useSelector(state => state.posts.posts);
  useEffect(() => {
    // dispatch(getPosts());
  } , [])
  return (
   <>
    <Head>
    <title>Test Next Js</title>
    </Head>
     
      <div className="container">
       <Table posts={posts}></Table>
      </div>
   </>
  )
}
export const getServerSideProps = wrapper.getServerSideProps((store) => {
  async () => {
    if (!store.getState().posts) {
      store.dispatch(getPosts())
      store.dispatch(END)
    }
  
    await store.sagaTask.toPromise()
  };

  

});

export default Home
