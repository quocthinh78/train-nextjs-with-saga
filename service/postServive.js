import axios from "axios";
import { baseUrl } from "./../constant/postAPi";
const key = "posts";
export const getPost = () => {
    return axios.get(`${baseUrl}/${key}`);
}