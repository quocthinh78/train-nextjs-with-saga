import React from "react"

 function Table ({posts}){
  Table.defaultProps = {
    posts : []
  }
    return (
        <table className="table mt-4">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Image</th>
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
          </tr>
        </thead>
        <tbody>
          {/* {
            posts.map(item => {
              return (
                <tr key={item.id}>
                  <th scope="row">{item.id}</th>
                  <td>{item.userId}</td>
                  <td>{item.title}</td>
                  <td><button className="btn btn-success">Update</button></td>
                  <td><button className="btn btn-danger">Delete</button></td>
                </tr>
              )
            })
          } */}
        </tbody>
      </table>
    )
}

export async function getStaticProps() {
  const res = await fetch('https://jsonplaceholder.typicode.com/posts')
  const products = await res.json()
  return {
    props: {
      products,
    },
  }
}

export default Table