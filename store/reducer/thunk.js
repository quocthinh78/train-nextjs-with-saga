import * as types from "../types";

const initialState = {
    posts: [],
    post: {},
    error: null
}

export const thunkReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_POST:
            return {
                ...state,
                posts: action.payload
            }

        default:
            return {
                ...state
            }
    }
}

export default thunkReducer;